import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Persona } from '../form-login/persona.model';

@Injectable({
  providedIn: 'root'
})
export class ServicioService {

  constructor(private http:HttpClient) { }

  getLogin(){
    return this.http.get('http://afa02ad4e64334d92b3e8bd8f249c28a-2139825710.us-east-1.elb.amazonaws.com:8085/auth/v1/tperson/');
  }

  login(persona:Persona){
    return this.http.post('http://afa02ad4e64334d92b3e8bd8f249c28a-2139825710.us-east-1.elb.amazonaws.com:8085/auth/v1/login/',persona);
  }
}
