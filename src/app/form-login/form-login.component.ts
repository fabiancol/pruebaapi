import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ServicioService } from '../services/servicio.service';
import { tipoPersona } from './tipoPersona.model';
import { ReactiveFormsModule } from '@angular/forms';
import { Persona } from './persona.model';
import { from } from 'rxjs';


@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.css']
})
export class FormLoginComponent implements OnInit {
  tipopersona: tipoPersona[] = [];
  form: FormGroup;
  constructor(private dataService: ServicioService
  ) {

  }


  ngOnInit(): void {
    this.form = new FormGroup({
      tipoIdentificacion: new FormControl('', [Validators.required]),
      numeroidentificacion: new FormControl('', [Validators.required]),
      contrasenia: new FormControl('', [Validators.required])
    }

    );
    this.dataService.getLogin().subscribe(
      (response: any) => {
        this.tipopersona = response as tipoPersona[];
        console.log(response);
      }
    )
  }
  enviar() {
    if (this.form.valid) {


      let person: Persona = {
        numeroidentificacion: this.form.get('numeroidentificacion')?.value,
        password: this.form.get('contrasenia')?.value,
        tipoidentificacion: this.form.get('tipoIdentificacion')?.value
      };
  console.log(person);
  this.dataService.login(person).subscribe(
    r=>{alert('login exitoso')},
    err=>{alert('Credenciales incorrectas')}
  );


    }
  }
}
